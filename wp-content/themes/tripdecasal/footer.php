<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trip_de_Casal
 */
global $configuracao;
?>

	<!-- REDES SOCIAIS / NEWSLETTER -->
	<section class="newsletter">
		<div class="container">
			<div class="row" style="display: ">

				<!-- REDES SOCIAIS -->
				<div class="col-md-5">

					<div class="redes-sociais-footer">
						<a href="<?php echo $configuracao['opt-facebook']; ?>" target="_blank" alt="facebook" title="facebook"><i class="fa fa-facebook-square"></i></a>
						<a href="<?php echo $configuracao['opt-instagram']; ?>" target="_blank" alt="instagram" title="instagram"><i class="fa fa-instagram"></i></a>
						<a href="<?php echo $configuracao['opt-youtube']; ?>" target="_blank" alt="youtube" title="youtube"><i class="fa fa-youtube-play"></i></a>
						<p>
							<?php echo $configuracao['opt-frase-footer']; ?>
						</p>
					</div>



				</div>

				<!-- FORMULÁRIO DE NEWALETTER -->
				<div class="col-md-7">
					<div class="form">
						<p>Quer saber mais sobre os nossos príximos destinos ?</p>
						<b>Assine a nossa newsletter.</b>
						<script type="text/javascript" src="http://tripdecasal.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.1"></script>
						<script type="text/javascript" src="http://tripdecasal.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.1"></script>
						<script type="text/javascript" src="http://tripdecasal.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.1"></script>
						<script type="text/javascript" src="http://tripdecasal.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
						<script type="text/javascript">
							var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://tripdecasal.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
						</script><script type="text/javascript" src="http://tripdecasal.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>


						<div class="widget_wysija_cont html_wysija">
							<div id="msg-form-wysija-html5710f6b323e8c-5" class="wysija-msg ajax"></div>
							<form id="form-wysija-html5710f6b323e8c-5" method="post" action="#wysija" class="widget_wysija html_wysija">
								<p class="wysija-paragraph">


								<input type="text" name="wysija[user][firstname]" class="campo-text wysija-input validate[required]" title="Seu nome" placeholder="Seu nome" value="" />



									<span class="abs-req">
										<input type="text" name="wysija[user][abs][firstname]" class="campo-text wysija-input validated[abs][firstname]" value="" />
									</span>

								</p>
								<p class="wysija-paragraph">


									<input type="text" name="wysija[user][email]" class="campo-text wysija-input validate[required,custom[email]]" title="Seu e-mail" placeholder="Seu e-mail" value="" />



									<span class="abs-req">
										<input type="text" name="wysija[user][abs][email]" class="campo-text wysija-input validated[abs][email]" value="" />
									</span>

								</p>
								<input class="botao wysija-submit wysija-submit-field" type="submit" value="Enviar" />

								<input type="hidden" name="form_id" value="5" />
								<input type="hidden" name="action" value="save" />
								<input type="hidden" name="controller" value="subscribers" />
								<input type="hidden" value="1" name="wysija-page" />


								<input type="hidden" name="wysija[user_list][list_ids]" value="3" />

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- FOOTER -->
	<footer class="rodape" style="display: ">
		<div class="container">
			<section class="row">

				<div class="col-md-3">

					<div class="logo">
						<!--LOGO  -->
						<a href="<?php echo site_url('/'); ?>" class="link-logo" title="Trip de Casal">
							<h1>
								Trip de casal
							</h1>
						</a>
					</div>

				</div>

				<div class="col-md-9">
					<div class="mapa-footer">
						<span>Mapa do site</span>
						<!-- MAPA -->
						<ul class="mapa">
							<!-- MENU -->
							<li>

								<small><b>Quem faz</b></small>
								<a href="<?php echo site_url('/origem-do-canal/'); ?>">Origem do canal</a>
								<a href="<?php echo site_url('/bruno/'); ?>">O Bruno Batuta</a>
								<a href="<?php echo site_url('/leticia/'); ?>">A Lê Mueller</a>

							</li>

							<!-- MENU -->
							<li>

								<a href="<?php echo site_url('/lugares/'); ?>"><b>Lugares</b></a>
								<a href="<?php echo site_url('/lugares/#ancora-formulario'); ?>" id="ancora-formulario">Localizar</a>
								<a href="<?php echo site_url('/lugares/#Visitados'); ?>">Visitados</a>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Indicações</button>

							</li>

							<!-- MENU -->
							<li>

								<small><b>Galeria</b></small>
								<a href="<?php echo site_url('galeria/'); ?>">Fotos</a>
								<a href="<?php echo site_url('video/'); ?>">Vídeos</a>

							</li>

							<!-- MENU -->
							<li>

								<a href="<?php echo site_url('/tripdecasal/'); ?>"><b>#TripDeCasal</b></a>
								<a href="<?php echo site_url('blog/'); ?>">Blog</a>
								<a href="<?php echo site_url('/parceiros/'); ?>"><b>Parceiros</b></a>


							</li>

							<!-- MENU -->
							<li>

								<small><b>Viajando na música</b></small>
								<a href="<?php echo site_url('/viajandomusica/'); ?>">Bandas</a>


							</li>

							<!-- MENU -->
							<li>

								<a href="<?php echo site_url('contato/'); ?>"><b>Contato</b></a>
								<a href=""></a>
								<a href=""></a>
								<a href=""></a>
							</li>



						</ul>

					</div>
				</div>

			</section>

			<div class="col-md-6">
 				<div class="copyright">
					<p>Copyright @ 2015 Trip de Casal. Todos os direitos reservados.</p>
				</div>
			</div>
			<div class="col-md-6 text-center">
			  <small>
			      <span style="vertical-align: text-bottom;color:#CECBCB;">Desenvolvido por</span>
			      <a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline-block;" ><img style="max-height: 15px;" src="<?php bloginfo('template_directory'); ?>/img/palupaLogo.png"></a>
			  </small>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>
