<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Trip_de_Casal
 */
global $configuracao;
global $post;
get_header(); ?>
	<?php
		$galeria = $configuracao['opt-bg-videos']['url'];
		$titulo =  $configuracao['opt-bg-titulo-videos'];

	?>
	<span class="info-pagina" data-link="<?php echo $galeria ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA LUGARES -->
	<div class="pg pg-lugares">

		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">

					<!-- POST -->
					<div class="postagem">
					<?php

						while ( have_posts() ) : the_post();
						$foto_destacada = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto_destacada= $foto_destacada[0];
						endwhile;
					 ?>
						<div class="video">
						 	<div class="embed-responsive embed-responsive-16by9 iframe">

						 		<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo rwmb_meta('Tripdecasal_link_video'); ?>"></iframe>

						 	</div>
						</div>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<!-- <div class="bg-post" style="background:url(<?php echo $foto_destacada?>);"></div> -->

							<div class="caixa">
								<div class="row">

									<div class="col-md-5 posicao">
										<!-- INFO -->
										<span>Destino</span>
										<!-- NOME DO POST -->
										<p class="info"><?php echo get_the_title() ?></p>
										<!-- LINK PARA O POST -->

									</div>

									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto"><?php echo get_the_content() ?></p>

									</div>

								</div>
							</div>
						</div>


					</div>

					<!-- <div class="video">
						 <div class="embed-responsive embed-responsive-16by9 iframe">

                       		<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo rwmb_meta('Tripdecasal_link_video'); ?>"></iframe>

                  		 </div>
					</div> -->

					<p class="title-pg"><i>Galeria de vídeos</i></p>
					<span class="title-pg2">Veja outros vídeos</span>

					<div class="imagens">
						<ul id="todas-fotos-album">
							  <?php

		                        $parceirosPost = new WP_Query( array( 'post_type' => 'video', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -4 ) );
		                        while ( $parceirosPost->have_posts() ) : $parceirosPost->the_post();

		                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		                        $foto = $foto[0];
							 ?>
							 <!-- IMAGENS DA GALERIA -->
							<li class="item">
								<a href="<?php echo get_permalink(); ?>">
									<span>

										<div class="bg-lugar" style="background: url(<?php echo $foto; ?>)no-repeat;"  title="<?php echo get_the_title() ?>"></div>

									</span>
								</a>
							</li>

							   <?php endwhile; ?>

						</ul>

						<?php
							//If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
					</div>

				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
					<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
						<!-- VÍDIO -->
						<div class="item">
							<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

						</div>

					<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php

get_footer();
