<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trip_de_Casal
 */

get_header(); ?>
	<?php
		$galeria = $configuracao['opt-bg-galeria']['url'];
		$titulo =  $configuracao['opt-bg-titulo-galeria'];

	?>
	<span class="info-pagina" data-link="<?php echo $galeria ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA LUGARES -->
	<div class="pg pg-galeria-lugares">

		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">
					<p><i>Galeria de fotos</i></p>
					<span>filtro de busca</span>

					<div class="form-pesquisa">
					  <!-- FORMULÁRIO DE REFINAMENTO -->
                        <form method="post">

                            <div class="form-group">
                                <select id="selectPais" class="text-pesquisa form-control"  name="Filtropais_galeria">
                                    <option value="">País</option>
	                                    <?php

	                                    	global $post;
					                        $pais = new WP_Query( array( 'post_type' => 'pais', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                        while ( $pais->have_posts() ) : $pais->the_post();

					                        $nomedoPais = get_the_title();
					                    ?>

                                    	<option data-idPais="<?php echo $post->ID; ?>" value="<?php echo $post->ID; ?>"><?php echo $nomedoPais ?></option>



	                                    <?php endwhile; wp_reset_query();?>
                                </select>
                            </div>

							<div class="form-group">

								<select id="selectDestino" class="text-pesquisa form-control"  name="FiltroDestino_galeria">
                                    <option value="">Destinos</option>
	                                    <?php
	                                    	global $post;
					                        $destino = new WP_Query( array( 'post_type' => 'destino', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                        while ( $destino->have_posts() ) : $destino->the_post();

					                        $nomedoDestino = get_the_title();

					                        $idPais = rwmb_meta('Tripdecasal_lista_pais');
					                    ?>

	                                    <option data-idDestino="<?php echo $idPais; ?>" value="<?php echo $post->ID ?>"><?php echo $nomedoDestino ?></option>



	                                    <?php endwhile; wp_reset_query();?>
                                </select>
							</div>

                            <button type="submit" class="botao-search"><i class="fa fa-search"></i></button>

                        </form>


	               		<?php if($wp_query->post_count > 0): ?>
	                	<span>Resultados encontrados: <?php echo $wp_query->post_count; ?> resultados</span>
	               		<?php endif; ?>

					</div>

					<p><i>Galeria de fotos</i></p>
					<span>Mapa de viagens</span>
					<!-- IMAGEM DO MAPA -->
					<div class="mapa">
						<?php echo $configuracao['opt-mapa']; ?>
					</div>

					<p><i>Galeria de fotos</i></p>
					<span>Buscar por álbuns</span>

					<!-- ÁLBUNS -->
					<div class="imagens">
						<ul>
							<!-- LOOP DE ÁLBUNS -->
							<?php if ( have_posts() ) : ?>
								<?php
								while ( have_posts() ) : the_post();

	                         	$galeriafoto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                       	 		$galeriafoto = $galeriafoto[0];

                            ?>
                            <!-- ÁLBUM -->
							<li>
								<a href="<?php echo get_permalink(); ?>" alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>">
									<div class="bg-lugar" style="background: url(<?php echo $galeriafoto ?>)no-repeat;"></div>
								</a>
								<i><?php echo get_the_title() ?></i>
							</li>
							<?php endwhile; ?>

							<?php else: ?>

							<h4>Que pena, não encontramos nenhuma galeria na localização buscada! :(</h4>

							<?php endif; ?>
						</ul>
					</div>

				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php

get_footer();
