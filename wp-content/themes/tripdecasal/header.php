<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trip_de_Casal
 */
global $configuracao;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
</head>

<body <?php body_class(); ?>>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-67616006-14', 'auto');
		ga('send', 'pageview');

	</script>
	<!-- REDES SOCIAIS -->
	<div class="redes-sociais">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="incon">
						<a href="<?php echo $configuracao['opt-facebook']; ?>" alt="facebook" title="facebook" target="_blank"><i class="fa fa-facebook-square"></i></a>
						<a href="<?php echo $configuracao['opt-instagram']; ?>" alt="instagram" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
						<a href="<?php echo $configuracao['opt-youtube']; ?>" alt="youtube" title="youtube" target="_blank"><i class="fa fa-youtube-play"></i></a>
					</div>
				</div>
				<div class="col-md-9">
					<p><?php echo $configuracao['opt-novidades']; ?></p>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-fundo-top">
		<div class="container">

			<!-- HEADER -->
			<header class="row topo">

				<div class="col-md-4 bg-menu">

					<!--LOGO  -->
					<a href="<?php echo site_url('/'); ?>" class="link-logo" title="Trip de casal">
						<h1>Trip de casal</h1>
					</a>

					<div class="posicao">
						<!-- IMG -->
						<div class="pontilhado"></div>
						<!-- MENU  -->
						<div class="navbar" role="navigation">

							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">

								<nav class="collapse navbar-collapse" id="collapse">

									<ul class="nav navbar-nav">

										<li class="dropdown">
											<a href="<?php echo site_url('/'); ?>" class="dropdown-toggle" id="home" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
										</li>

										<li class="dropdown">
											<a href="#" class="dropdown-toggle" id="quem-faz" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quem faz</a>
											<ul class="dropdown-menu">
												<li><a href="<?php echo site_url('/origem-do-canal/'); ?>" id="origemdocanal" alt="Origem do canal">Origem do canal</a></li>
												<li><a href="<?php echo site_url('/bruno/'); ?>" id="bruno" alt="O Bruno Batuta">O Bruno Batuta</a></li>
												<li><a href="<?php echo site_url('/leticia/'); ?>" id="leticia" alt="A Lê Mueller">A Lê Mueller</a></li>
											</ul>
										</li>

										<!-- <li><a href="#">Blog</a></li> -->

										<!-- <li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cursos</a>
										</li> -->

										<li><a href="<?php echo site_url('/lugares/'); ?>" id="lugar" alt="Lugares">Lugares</a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" id="galeria" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Galeria</a>
											<ul class="dropdown-menu">
												<li><a href="<?php echo site_url('galeria/'); ?>" id="fotos" alt="Fotos">Fotos</a></li>
												<li><a href="<?php echo site_url('video/'); ?>" id="video" alt="Vídeos">Vídeos</a></li>
											</ul>
										</li>

										<li><a href="<?php echo site_url('/tripdecasal/'); ?>" id="tripdeCasal" alt="tripdeCasal">#TripDeCasal</a></li>
										<li><a href="<?php echo site_url('blog/'); ?>" id="blog" alt="blog">Blog</a></li>
										<li><a href="<?php echo site_url('/parceiros/'); ?>" id="parceiros" alt="Parceiros">Parceiros</a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" id="viajandonamusica" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Viajando na música</a>
											<ul class="dropdown-menu">
												<li><a href="<?php echo site_url('/trilha-sonora/'); ?>" id="playlists" alt="Playlists">Playlists</a></li>
												<li><a href="<?php echo site_url('/viajandomusica/'); ?>" id="bandas" alt="Bandas">Bandas</a></li>
											</ul>
										</li>
										<li><a href="<?php echo site_url('contato/'); ?>" id="contato"alt="Contato">Contato</a></li>

									</ul>

								</nav>

							</div>

						</div>

						<!-- ÁREA DE PESQUISA -->
						<div class="busca-site">
							<!-- <input type="text" placeholder="buscar no site" class="campo-pesquisa"> -->
							<!-- <button type="submit" class="botao-de-presquisa" ><i class="fa fa-search"></i></button> -->
							<form method="get" id="searchform" action="<?php echo home_url('/'); ?>" >
								<input type="text" placeholder="buscar no site"  class="field campo-pesquisa" name="s" id="s" >
								<!-- <input type="submit" class="btn enviar" name="submit" id="searchsubmit" value=""> -->
								<button type="submit" class="botao-de-presquisa" name="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
							</form>
						</div>

					</div>

				</div>

				<div class="col-md-8 altura">
					<div class="caixa-texto" id="caixadetexto">
						<span class="textos"><?php echo $configuracao['opt-titulo']; ?></span>
						<b class="textos"><?php echo $configuracao['opt-frase-top']; ?></b>
						<p class="textos"> <?php echo $configuracao['opt-texto']; ?></p>
					</div>
					<!-- BG PÁGINA INICIAL -->

					<div  id="foto-top" class="bg-header">
						<p id="frase"></p>
					</div>


					<p id="titulo" class="legenda"></p>
				</div>

			</header>

		</div>
	</div>



	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-body modal-topo">
	    	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      		<p>Indique um local ao Trip de Casal </p><i class="fa fa-street-view"></i>
				<span class="info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </span>
		      	<div class="form-indicacao">

					<?php
	                   echo do_shortcode('[contact-form-7 id="239" title="Formulário de indicações de local"]');
	                ?>
				</div>
	      	</div>
	    </div>
	  </div>
	</div>