<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Trip_de_Casal
 */
global $configuracao;
global $post;
get_header(); ?>
	<?php
		$galeria = $configuracao['opt-bg-galeria']['url'];
		$titulo =  $configuracao['opt-bg-titulo-galeria'];

	?>
	<span class="info-pagina" data-link="<?php echo $galeria ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA LUGARES -->
	<div class="pg pg-lugares" style="display:">

		<!-- MODAL DOS ÁLBUNS -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		  <div class="modal-dialog" role="document">

		    <div class="modal-content modal-galeria">

		      <div class="modal-body">

	     		<!-- PÁGINA DE ÁLBUNS -->
				<div class="pg pg-albuns">
					<div class="container">
						<div class="bg-fundo">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<!-- CARROSSEL DE ÁLBUSN -->
							<div class="carrossel">
								<div id="" class="">
									<div class="item">
										<div class="foto" id="fotodestaque">
											<div class="descricao">
												<p id="tituloPost"><?php 	echo get_the_title() ?></p>
											</div>
											<div class="areaBtnCarrossel">
												<button id="btnPrev" class="btnPrev hidden-xs"><i class="fa fa-angle-left"></i></button>
												<button id="btnNext" class="btnNext hidden-xs"><i class="fa fa-angle-right"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- CARROSSEL DE ÁLBUSN -->
							<div class="carrossel-albuns">
								<div id="carrossel-albuns" class="owl-Carousel">

									<?php


										$fotosModal = rwmb_meta('Tripdecasal_image_galeria','type=image' );

										foreach ($fotosModal as $fotoModal) {


									 ?>
									<div class="item">
										<div class="foto-carrossel" id="primeira-foto" data-url="<?php echo $fotoModal['full_url']; ?>" data-titulo="<?php echo get_the_title() ?>" style="background: url(<?php echo $fotoModal['full_url']; ?>);">

										</div>
									</div>

									<?php } ?>

								</div>

								<!-- BOTÕES DE NAVEGAÇÃO -->
								<div class="botoes-produtosingle">
									<button class="navegacaoalbunsFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
									<button class="navegacaoalbunsTras hidden-xs"><i class="fa fa-angle-right"></i></button>
								</div>
							</div>

							 <p class="titulo">Outros álbuns</p>

							<div class="outros-albuns">
								<ul>
								<?php

		                            $albuns = new WP_Query( array( 'post_type' => 'galeria', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 6 ) );

		                            while ( $albuns->have_posts() ) : $albuns->the_post();
		                         	$fotoalbuns = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                       	 		$fotoalbuns = $fotoalbuns[0];

                       	 				$fotosdoAlbum = rwmb_meta('Tripdecasal_image_galeria');
	                            ?>

									<li>
										<a href="<?php echo get_permalink(); ?>">
											<div class="album" style="background: url(<?php echo $fotoalbuns ?>);"></div>
										</a>
										<p><?php echo get_the_title() ?></p>
									</li>
								<?php endwhile; ?>
								</ul>

							</div>
						</div>

					</div>

				</div>


		      </div>

		    </div>

		  </div>

		</div>

		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">

					<!-- POST -->
					<div class="postagem">
					<?php

						while ( have_posts() ) : the_post();
						$foto_destacada = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto_destacada= $foto_destacada[0];
						endwhile;
					 ?>

						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<div class="bg-post" style="background:url(<?php echo $foto_destacada?>);"></div>

							<div class="caixa">
								<div class="row">

									<div class="col-md-5 posicao">
										<!-- INFO -->
										<span>Destino</span>
										<!-- NOME DO POST -->
										<p class="info"><?php echo get_the_title() ?></p>
										<!-- LINK PARA O POST -->

									</div>

									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto"><?php echo get_the_content() ?></p>

									</div>

								</div>
							</div>
						</div>


					</div>

					<span class="info-fotos">Clique nas imagens abaixo para visualizar</span>
					<div class="imagens">
						<ul id="todas-fotos-album">
							<?php
								$fotos = rwmb_meta('Tripdecasal_image_galeria','type=image' );

								foreach ($fotos as $foto) {


							 ?>
							 <!-- IMAGENS DA GALERIA -->
							<li class="item">
								<span class="link-album" data-foto-destacada="<?php echo $foto_destacada?>" data-title="<?php echo get_the_title() ?>">

									<div class="bg-lugar" style="background: url(<?php echo $foto['full_url']; ?>)no-repeat;" title="<?php echo get_the_title() ?>"></div>

								</span>
							</li>

							<?php } ?>

						</ul>
					</div>

					<?php
							//If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php

get_footer();
