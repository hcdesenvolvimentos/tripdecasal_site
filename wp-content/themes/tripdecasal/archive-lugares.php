<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trip_de_Casal
 */

global $configuracao;
global $post;
get_header(); ?>

	<script>

	/*****************************************
		*  	SCROLL SUAVE LINK MESMA PÁGINA
	*****************************************/
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

	</script>
	<?php
		$lugares = $configuracao['opt-bg-lugares']['url'];
		$titulo =  $configuracao['opt-bg-titulo-lugares'];

	 ?>
	<span id="ancora-formulario" class="info-pagina" data-link="<?php echo $lugares ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA LUGARES -->
	<div class="pg pg-categoria-lugares">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">
					<p><i>Lugares</i></p>
					<span>Buscar por localidade</span>

					<div class="form-pesquisa">
					  <!-- FORMULÁRIO DE REFINAMENTO -->
                        <form method="post">

                            <div class="form-group">
                                <select id="selectPais" class="text-pesquisa form-control"  name="poFiltroPais">
                                    <option value="">País</option>
	                                    <?php

	                                    	global $post;
					                        $pais = new WP_Query( array( 'post_type' => 'pais', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                        while ( $pais->have_posts() ) : $pais->the_post();

					                        $nomedoPais = get_the_title();
					                    ?>

                                    	<option data-idPais="<?php echo $post->ID; ?>" value="<?php echo $post->ID; ?>"><?php echo $nomedoPais ?></option>



	                                    <?php endwhile; wp_reset_query();?>
                                </select>
                            </div>

							<div class="form-group">

								<select id="selectDestino" class="text-pesquisa form-control"  name="poFiltroDestino">
                                    <option value="">Destinos</option>
	                                    <?php
	                                    	global $post;
					                        $destino = new WP_Query( array( 'post_type' => 'destino', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                        while ( $destino->have_posts() ) : $destino->the_post();

					                        $nomedoDestino = get_the_title();

					                        $idPais = rwmb_meta('Tripdecasal_lista_pais');
					                    ?>

	                                    <option data-idDestino="<?php echo $idPais; ?>" value="<?php echo $post->ID ?>"><?php echo $nomedoDestino ?></option>



	                                    <?php endwhile; wp_reset_query();?>
                                </select>
							</div>

                            <button type="submit" class="botao-search"><i class="fa fa-search"></i></button>

                        </form>


	               		<?php if($wp_query->post_count > 0): ?>
	                	<span>Resultados encontrados: <?php echo $wp_query->post_count; ?> resultados</span>
	               		<?php endif; ?>

					</div>

					<p><i>Lugares</i></p>
					<span>categorias</span>

					<div class="imagens">
						<ul>

							<?php

								// DEFINE A TAXONOMIA
								$taxonomia = 'categoriaLugares';

								// LISTA AS CATEGORIAS PAI DOS SABORES
								$categoriaLugares = get_terms( $taxonomia, array(
									'orderby'    => 'count',
									'hide_empty' => 0,
									'parent'	 => 0
								));



								foreach ($categoriaLugares as $categoriaLugar) {
									$nome = $categoriaLugar->name;
									$categoriaLugarimg = z_taxonomy_image_url($categoriaLugar->term_id);


							?>
								<li>
									<a href="<?php echo get_category_link($categoriaLugar->term_id); ?>">
										<div class="bg-lugar" style="background: url(<?php echo $categoriaLugarimg; ?>)no-repeat;"></div>
									</a>
									<i ><?php echo $nome; ?></i>
								</li>

							<?php } ?>
						</ul>
					</div>

					<p><i id="Visitados">Lugares</i></p>
					<span>Últimas recomendações</span>

					<!-- POST -->
					<div class="postagem">
						<?php if ( have_posts() ) : ?>
							<?php

								while ( have_posts() ) : the_post();

		                        // $lugaresrecomnedados = new WP_Query( array( 'post_type' => 'lugares', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -5 ) );

		                        // while ( $lugaresrecomnedados->have_posts() ) : $lugaresrecomnedados->the_post();
		                     	$fotodolugar = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		               	 		$fotodolugar = $fotodolugar[0];

		                    ?>

							<div class="post">
								<!-- IMAGEM DESTACADA -->
								<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>">
									<div class="bg-post" style="background:url(<?php echo $fotodolugar ?>);"></div>
								</a>

								<div class="caixa">
									<div class="row">

										<div class="col-md-5 posicao">
											<!-- INFO -->

											<!-- <span>Destino</span> -->
											<!-- NOME DO POST -->
											<p class="info"><?php echo get_the_title() ?></p>
											<!-- LINK PARA O POST -->
											<a href="<?php echo get_permalink(); ?>" class="efeito" >Saiba mais</a>
										</div>

										<div class="col-md-7">
											<!-- DESCRIÇÃO -->
											<p class="texto">
											<?php
												$content = get_the_content();
												$conteudo = substr($content, 0, 80).'...';
												echo $conteudo;
											?></p>

										</div>

									</div>
								</div>
							</div>

							<?php endwhile; ?>

						<?php else: ?>

						<h4>Que pena, não encontramos nenhum destino na localização buscada! :(</h4>

						<?php endif; ?>

					</div>

				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>


<?php

get_footer();
