<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Trip_de_Casal
 */

get_header(); ?>
	<?php 	
		$post = $configuracao['opt-bg-post']['url'];
		$titulo =  $configuracao['opt-bg-inicial-titulo'];
		
 	?>
	<span class="info-pagina" data-link="<?php echo $post ?>" data-title="<?php echo $titulo ?>"></span>

		<!-- PÁGINA POSTAGEM -->
	<div class="pg pg-postagem">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">
				
				<?php get_sidebar() ?>
				
				<!-- POSTS -->
				<div class="col-md-8">
					
					<header class="page-header">
						<h1 id="result-pesquisa" class="page-title"><?php printf( esc_html__( 'Nenhum resultado de pesquisa: %s', 'tripdecasal' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					</header><!-- .page-header -->
					<!-- POST -->
					<div class="postagem" id="postagem-pesquisa">
						

						<?php 
							while ( have_posts() ) : the_post();		
								$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$foto = $foto[0];
							endwhile;
						 ?>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<div class="bg-post" style="background:url(<?php echo $foto ?>);"></div>
							
							<div class="caixa">
								<div class="row">
									
									<div class="col-md-12 posicao text-center">
										<div class="" id="result-pesquisa2">
											<p>Não encontramos nenhum resultado :(</p>
										</div>

										<a href="<?php echo site_url('/'); ?>" class="botoes-pagina-pesquisa">Voltar a página inicial</a>
										
									</div>
								
								</div>
							</div>
						</div>

					</div>					
					
				</div>

			</section>		
			
		</div>
	</div>
	<!-- CARROSSEL -->		
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">						
					<div class="bg"></div>		
				</div>	
				<div class="col-md-11">	
					<p>#Trip de casal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">					
						<div id="carrossel-videos" class="owl-Carousel">
							<?php 
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {			        		
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>		        		
								
							</div>

							<?php 	
			       		}
					?>
						</div>
					
					</div>				
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
