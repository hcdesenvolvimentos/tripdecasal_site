<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trip_de_Casal
 */
global $configuracao;
global $post;
get_header(); ?>
	<?php
		$viajandomusica = $configuracao['opt-bg-viajando']['url'];
		$titulo =  $configuracao['opt-bg-tituloviajando'];
	 ?>
	<span class="info-pagina" data-link="<?php echo $viajandomusica ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA PLAYLISTS -->
	<div class="pg pg-playlist">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">
					<p><i>Viajando na música </i></p>
					<span>Busca de playlists</span>
					<div class="form-pesquisa">
					  <!-- FORMULÁRIO DE REFINAMENTO -->
                        <form method="post">

                            <div class="form-group">
                                <select  class="text-pesquisa form-control"  name="Filtro_banda">
                                    <option value="">Buscar por banda</option>
	                                    <?php

	                                    	global $post;
					                        $banda = new WP_Query( array( 'post_type' => 'banda', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                        while ( $banda->have_posts() ) : $banda->the_post();

					                        $Nomebanda = get_the_title();
					                    ?>

                                    	<option data-idPais="<?php echo $post->ID; ?>" value="<?php echo $post->ID; ?>"><?php echo $Nomebanda ?></option>



	                                    <?php endwhile; wp_reset_query();?>
                                </select>
                            </div>

						<!-- 	<div class="form-group">

								<select class="text-pesquisa form-control"  name="FiltroDestino_banda">
                                    <option value="">Destinos</option> -->
	                                    <?php
	                                    	//global $post;
					                        //$destino = new WP_Query( array( 'post_type' => 'destino', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

					                       // while ( $destino->have_posts() ) : $destino->the_post();

					                       // $nomedoDestino = get_the_title();

					                       // $idPais = rwmb_meta('Tripdecasal_lista_destinos_banda');
					                    ?>

	                                    <!-- <option data-idDestino="<?php //echo $idPais; ?>" value="<?php //echo $post->ID ?>"><?php //echo $nomedoDestino ?></option> -->



	                                    <?php //endwhile; wp_reset_query();?>
                               <!--  </select>
							</div> -->

                            <button type="submit" class="botao-search"><i class="fa fa-search"></i></button>

                        </form>


	               		<?php if($wp_query->post_count > 0): ?>
	                	<span>Resultados encontrados: <?php echo $wp_query->post_count; ?> resultados</span>
	               		<?php endif; ?>

					</div>

					<!-- POST -->
					<div class="postagem">
						<p><i>Viajando na música</i></p>
						<span>Últimas Playlists</span>

						<?php if ( have_posts() ) : ?>

								<?php while ( have_posts() ) : the_post();

                         	$fotobandas = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                   	 		$fotobandas = $fotobandas[0];

                        ?>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<div class="bg-post" style="background:url(<?php echo $fotobandas  ?>);"></div>

							<div class="caixa">
								<div class="row">

									<div class="col-md-5 posicao">
										<!-- INFO -->
										<span class="trilha">Trilha sonora</span>
										<?php
											$destinoId = rwmb_meta('Tripdecasal_lista_destinos_banda');

									    	$destinoPost = new WP_Query( array( 'post_type' => 'destino', 'p' => $destinoId, 'posts_per_page' => -1 ) );


											while ( $destinoPost->have_posts() ) : $destinoPost->the_post();
										?>
										<!-- NOME DO POST -->
										<p class="info"><?php echo the_title();?></p>

										<?php endwhile; wp_reset_query(); ?>
										<!-- LINK PARA O POST -->
										<?php
											 $fotos = rwmb_meta('Tripdecasal_image_capa');

									 		foreach ($fotos as $foto) {


										  ?>
										<div class="bg-album" style="background: url(<?php echo $foto['full_url']; ?>);"></div>
										<?php } ?>
									</div>

									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto"><?php echo get_the_title() ?></p>
										<?php echo $script = rwmb_meta('Tripdecasal_album_script');  ?>
									</div>

								</div>
							</div>
						</div>
						<hr>
						<?php endwhile; ?>

							<?php else: ?>

							<h4>Que pena, não encontramos nenhuma trilha sonora na localização buscada! :(</h4>

							<?php endif; ?>

					</div>


				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_sidebar();
get_footer();
