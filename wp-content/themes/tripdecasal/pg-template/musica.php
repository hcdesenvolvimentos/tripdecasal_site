

<?php
/**
 * Template Name: Viajando na música
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>
<?php
	$post = $configuracao['opt-bg-viajando']['url'];
	$titulo =  $configuracao['opt-bg-tituloviajando'];

 ?>
	<span class="info-pagina" data-link="<?php echo $post ?>" data-title="<?php echo $titulo ?>"></span>

<!-- TRIP DE CASAL -->
	<div class="pg pg-musica">

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	     		<p><i>Viajando na música</i></p>

				<span class="info">Seja nosso parceiro</span>

				<p>Faça parte de uma trilha sonora do canal!</p>

	     		<div class="form-banda">


					<?php
	                   echo do_shortcode('[contact-form-7 id="238" title="Formulário de cadastro das bandas"]');
	                ?>

	     		</div>
		      </div>
		    </div>
		  </div>
		</div>

		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">
				<!-- LOOP THE CONTENT -->
						<?php if ( have_posts() ) : while( have_posts() ) : the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                        $foto = $foto[0];


	                    endwhile; endif; ?>
					<p class="sobre">
						<?php  echo get_the_content() ?>
					</p>


					<div class="bg-musica" style="background: url(<?php echo $foto ?>);"></div>

					<button  data-toggle="modal" data-target="#myModal" class="link-facaparte">Faça parte de uma trilha sonora do canal</button>

					<div class="conteudo">
						<p><i>Viajando na música</i></p>
						<span>Bandas Parceiras</span>
						<ul class="parceiros">
							<?php

	                            $bandas = new WP_Query( array( 'post_type' => 'banda', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

	                            while ( $bandas->have_posts() ) : $bandas->the_post();
	                         	$fotobandas = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                       	 		$fotobandas = $fotobandas[0];

                            ?>
							<li>
								<a href="<?php echo rwmb_meta('Tripdecasal_link_banda'); ?>" target="_Blank">
									<img src="<?php echo $fotobandas ?>"  class="img-responsive" title="<?php echo get_the_title() ?>" alt="<?php echo get_the_title() ?>">
								</a>
								<i><?php echo get_the_title() ?></i>
							</li>
							<?php endwhile; ?>
						</ul>

					</div>
				</div>

			</section>

		</div>
	</div>

	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>