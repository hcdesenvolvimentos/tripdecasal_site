

<?php
/**
 * Template Name: Playlists
 * Description: 
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>
	<?php 	
		$viajandomusica = $configuracao['opt-bg-viajando']['url'];
		$titulo =  $configuracao['opt-bg-tituloviajando'];
	 ?>
	<span class="info-pagina" data-link="<?php echo $viajandomusica ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- PÁGINA PLAYLISTS -->
	<div class="pg pg-playlist">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">
				
				<?php get_sidebar() ?>
				
				<!-- POSTS -->
				<div class="col-md-8">
					
					<!-- POST -->
					<div class="postagem">
						<p><i>Viajando na música</i></p>
						<span>ùltimas Playlists</span>	

						<?php 

                            $bandas = new WP_Query( array( 'post_type' => 'trilha-sonora', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

                            while ( $bandas->have_posts() ) : $bandas->the_post();                           
                         	$fotobandas = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                   	 		$fotobandas = $fotobandas[0];

                        ?>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<div class="bg-post" style="background:url(<?php echo $fotobandas  ?>);"></div>
							
							<div class="caixa">
								<div class="row">
									
									<div class="col-md-5 posicao">
										<!-- INFO -->
										<span class="trilha">Trilha sonora</span>
										<!-- NOME DO POST -->
										<p class="info"><?php echo get_the_title() ?></p>
										<!-- LINK PARA O POST -->
										<?php 
											 $fotos = rwmb_meta('Tripdecasal_image_capa'); 
											 	var_dump($fotos);
									 		//foreach ($fotos as $foto) {
									 		
									 		
										  ?>
										<div class="bg-album" style="background: url(<?php //echo $foto['full_url']; ?>);"></div>
										<?php //} ?>
									</div>
									
									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto"><?php echo get_the_content() ?></p>
										<?php echo $script = rwmb_meta('Tripdecasal_album_script');  ?>
									</div>
								
								</div>
							</div>
						</div>
						<?php endwhile; ?>  

					</div>	


				</div>

			</section>		
			
		</div>
	</div>
	<!-- CARROSSEL -->		
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">						
					<div class="bg"></div>		
				</div>	
				<div class="col-md-11">	
					<p>#Trip de casal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">					
						<div id="carrossel-videos" class="owl-Carousel">
							<?php 
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {			        		
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>		        		
								
							</div>

							<?php 	
			       		}
					?>
						</div>
					
					</div>				
				</div>
			</div>
		</div>
	</section>


<?php get_footer(); ?>