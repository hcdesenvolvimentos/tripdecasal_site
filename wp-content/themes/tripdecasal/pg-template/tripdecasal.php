

<?php
/**
 * Template Name: #Tripdecasal
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>
	<?php
		$viajandomusica = $configuracao['opt-bg-tripcasal']['url'];
		$titulo =  $configuracao['opt-bg-titulo-tripcasal'];
	 ?>
	<span class="info-pagina" data-link="<?php echo $viajandomusica ?>" data-title="<?php echo $titulo ?>"></span>

<!-- TRIP DE CASAL -->
	<div class="pg pg-tripdecasal" >
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>



				<?php if ( have_posts() ) : while( have_posts() ) : the_post();
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    $foto = $foto[0];


                    endwhile; endif;
                ?>

				<!-- POSTS -->
				<div class="col-md-8">
					<p class="sobre"><?php echo  get_the_content() ?> </p>

				</div>

			</section>
			<div class="imagens" id="altura">
				<div class="pontilhado" id="pon"></div>
				<p>Instagram do #TripDeCasal</p>
				<?php
					echo do_shortcode('[alpine-phototile-for-instagram id=739 user="tripdecasal" src="user_recent" imgl="fancybox" style="wall" row="5" size="L" num="15" shadow="1" highlight="1" curve="1" align="center" max="100%" nocredit="1"]');
			       //echo do_shortcode('[alpine-phototile-for-instagram id=554 user="hudsoncarolino" src="user_recent" imgl="fancybox" style="wall" row="5" size="L" num="30" align="center" max="80"]');
			    ?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>