

<?php
/**
 * Template Name: Inicial
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;
global $post;

get_header(); ?>
	<?php
		$lugares = $configuracao['opt-bg-inicial']['url'];
		$titulo =  $configuracao['opt-bg-inicial-titulo'];

	 ?>
	<span class="info-pagina" data-link="<?php echo $lugares ?>" data-title="<?php echo $titulo ?>"></span>
	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">

					<!-- POST -->
					<div class="postagem">

						<?php //$the_query = new WP_Query( 'posts_per_page=-1' ); ?>

						<?php
							query_posts( array( 'posts_per_page' => 6, 'post_type' => array( 'post', 'lugares', 'galeria' ), 'paged' => get_query_var( 'paged' ) ) );

							if ( have_posts() ) : while( have_posts() ) : the_post();
							$post_type = get_post_type_object( get_post_type($post) );
							$postName = $post_type->label ;
							if ($postName == "Lugar") {
								$postName = "Lugares";
							}elseif ($postName == "Posts") {
								$postName = "Postagens";
							}
							//while ($the_query -> have_posts()) : $the_query -> the_post();
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
						 ?>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>">
								<div class="bg-post" style="background:url(<?php echo $foto  ?>);"></div>
							</a>

							<div class="caixa">
								<div class="row">

									<div class="col-md-5 posicao">
									<?php
										// CATEGORIA ATUAL
										$categoriaAtual = get_the_category();
										$categoriaAtuall = $categoriaAtual[0]->cat_name;

									?>
										<!-- INFO -->
										<span><?php  echo $postName; ?></span>
										<!-- NOME DO POST -->
										<p class="info"><?php echo get_the_title() ?></p>
										<!-- LINK PARA O POST -->
										<a href="<?php echo get_permalink(); ?>" class="efeito" >Saiba mais</a>
									</div>

									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto">

										<?php
											$content = get_the_content();
											$conteudo = substr($content, 0, 80).'...';
											echo $conteudo;
										?>
										</p>
									</div>

								</div>
							</div>
						</div>
						<?php
						endwhile;
						endif;
						wp_reset_postdata();
						?>

					</div>

				</div>

			</section>

		</div>
	</div>

	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>


<?php get_footer(); ?>