

<?php
/**
 * Template Name: Parceiros
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>
	<?php
		$parceiros = $configuracao['opt-bg-parceiros']['url'];
		$titulo =  $configuracao['opt-bg-titulo-parceiros'];

	?>
	<span class="info-pagina" data-link="<?php echo $parceiros ?>" data-title="<?php echo $titulo ?>"></span>

<!-- TRIP DE CASAL -->
	<div class="pg pg-parceiros" style="display:">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">
					<div class="conteudo">
						<p><i>Com quem já fizemos parceria</i></p>
						<?php

							// DEFINE A TAXONOMIA
							$taxonomia = 'categoriaParceiro';

							// LISTA AS CATEGORIAS PAI DOS PARCEIROS
							$listarParceiros = get_terms( $taxonomia, array(
								'orderby'    => 'count',
								'hide_empty' => 0,
								'parent'	 => 0
							));

							// PRA CADA PARCEIRO
							foreach ($listarParceiros as $parceiro):

						?>


						<span><?php echo $parceiro->name; ?></span>
							<ul class="parceiros">
								<?php

									// EXECUTA O LOOP DE ITENS DO CARDÁPIO DA RESPECTIVA SUBCATEGORIA
							    	$todosparceiros = new WP_Query( array(  'post_type' 		=> 'parceiro',
							    									 'posts_per_page' 	=> -1,
							    									 'tax_query' 		=> array(
																								array(
																									'taxonomy' => $taxonomia,
																									'field'    => 'slug',
																									'terms'    => $parceiro->slug,
																								)
								    														)
							    									)
							    							);

									// ENQUANTO HOUVER ITENS NO LOOP
									while ( $todosparceiros->have_posts() ) : $todosparceiros->the_post();
									$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$foto = $foto[0];
								?>

								<li>
									<a href="<?php echo rwmb_meta('Tripdecasal_link_parceiro'); ?>" target="_Blank">
										<img src="<?php echo $foto ?>"  class="img-responsive" title="<?php echo get_the_title();?>" alt="<?php echo get_the_title();?>">
									</a>
								</li>
								<?php
									endwhile;
								?>

							</ul>

							<?php

								endforeach;

							?>
					</div>
				</div>

			</section>

		</div>
	</div>
<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>