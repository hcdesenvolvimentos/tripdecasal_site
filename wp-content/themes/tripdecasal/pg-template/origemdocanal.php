

<?php
/**
 * Template Name: Origem do canal
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>

	<?php
		$bruno = $configuracao['opt-bg-origem']['url'];
		$titulo =  $configuracao['opt-bg-titulo-origem'];

	 ?>
	 <span class="info-pagina" data-frase="<?php echo $frase ?>" data-link="<?php echo $bruno ?>" data-title="<?php echo $titulo ?>"></span>


<!-- PÁGINA QUEM FAZ -->
<div class="pg pg-quem-faz">
	<div class="container">

		<!-- CONTEÚDO -->
		<section class="row">

			<?php get_sidebar() ?>

			<!-- POSTS -->
			<div class="col-md-8">

				<!-- POST -->
				<div class="postagem">

					<?php
					// LOOP DA FOTO DESTACADA
						if ( have_posts() ) : while( have_posts() ) : the_post();
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
							 endwhile; endif;
						 ?>
					<div class="post">
						<!-- IMAGEM DESTACADA -->
						<div class="bg-post" style="background:url(<?php echo $foto  ?>);"></div>

						<div class="caixa">
							<div class="row">

								<div class="col-md-5 posicao">
									<div class="pontilhado-postagem"></div>
									<!-- INFO -->
									<span>Quem faz</span>
									<!-- NOME DO POST -->
									<p class="info"><?php echo get_the_title() ?></p>

								</div>

								<div class="col-md-7">
									<!-- DESCRIÇÃO -->
									<p class="texto">
										<?php echo get_the_content(); ?>

   									</p>

								</div>

							</div>
						</div>
					</div>

				</div>

			</div>

		</section>

	</div>
</div>
<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>