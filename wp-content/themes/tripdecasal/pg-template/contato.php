

<?php
/**
 * Template Name: Contato
 * Description:
 *
 * @package Trip_de_Casal
 */
global $configuracao;

get_header(); ?>

	<?php
		$contato = $configuracao['opt-bg-contato']['url'];
		$titulo =  $configuracao['opt-bg-titulo-contato'];
	 ?>
	<span class="info-pagina" data-link="<?php echo $contato ?>" data-title="<?php echo $titulo ?>"></span>

	<!-- CONTATNO -->
	<div class="pg pg-contato" style="display:">
		<div class="container">
			<section class="row">

				<?php get_sidebar() ?>

				<div class="col-md-8 correcao">
					<?php
					// LOOP DA FOTO DESTACADA
					if ( have_posts() ) : while( have_posts() ) : the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						 endwhile; endif;
					 ?>
					<div class="bg-contato" style="background:url(<?php echo $foto ?>); "></div>

					<div class="info">

						<span>Redes Sociais</span>
						<p><?php echo $configuracao['opt-onde-encontrar']; ?></p>

						<div class="redes-sociais">
							<a href="<?php echo $configuracao['opt-facebook']; ?>" alt="facebook" title="facebook" target="_blank"><i class="fa fa-facebook-square"></i></a>
							<a href="<?php echo $configuracao['opt-instagram']; ?>" alt="instagram" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="<?php echo $configuracao['opt-youtube']; ?>" alt="youtube" title="youtube" target="_blank"><i class="fa fa-youtube-play"></i></a>
						</div>

						<div class="contato">
							<span>Outros contatos</span>
							<p>Tel. <?php echo $configuracao['opt-numero']; ?></p>
							<span><?php echo $configuracao['opt-email']; ?></span>
						</div>

					</div>

					<div class="form">
						<span><?php echo $configuracao['opt-onde-encontrar-frase']; ?></span>
						<p>Formulário de contato</p>
						<?php
		                   echo do_shortcode(' [contact-form-7 id="164" title="Formulário de contato"]');
		                ?>

					<!-- 	<div class="form-group">

							<label class="hidden" for="nome">Seu nome:</label>

							<input type="nome"  placeholder="Seu nome" class="form-control" id="nome">

						</div>

						<div class="form-group">

							<label class="hidden" for="email">Seu email:</label>

							<input type="email" placeholder="e-mail"  class="form-control" id="email">

						</div>

						<div class="form-group">

							<label class="hidden" for="menssagem"> menssagem:</label>

							<textarea name="" id="" placeholder="Digite sua mensagem aqui" ></textarea>

						</div>

						<input class="botao" type="submit"> -->

					</div>

				</div>

			</section>
		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>