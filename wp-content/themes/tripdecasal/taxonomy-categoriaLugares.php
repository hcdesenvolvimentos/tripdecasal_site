<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trip_de_Casal
 */
global $configuracao;
get_header(); ?>
	<?php
		$lugares = $configuracao['opt-bg-lugares']['url'];
		$titulo =  $configuracao['opt-bg-titulo-lugares'];

	 ?>
	<span class="info-pagina" data-link="<?php echo $lugares ?>" data-title="<?php echo $titulo ?>"></span>


<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		<div class="container">
			<!-- CONTEÚDO -->
			<section class="row">

				<?php get_sidebar() ?>

				<!-- POSTS -->
				<div class="col-md-8">

					<!-- POST -->
					<div class="postagem">

						<?php
							// LOOP DA FOTO DESTACADA
							if ( have_posts() ) : while( have_posts() ) : the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			                $foto = $foto[0];

						 ?>
						<div class="post">
							<!-- IMAGEM DESTACADA -->
							<div class="bg-post" style="background:url(<?php echo $foto  ?>);"></div>

							<div class="caixa">
								<div class="row">

									<div class="col-md-5 posicao">
										<?php
											$decricao = explode(";", $categoriaLugar->description);
											$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
										?>
										<!-- INFO -->
										<span><?php echo $term->name; ?></span>
										<!-- NOME DO POST -->
										<p class="info"><?php echo get_the_title() ?></p>
										<!-- LINK PARA O POST -->
										<a href="<?php echo get_permalink(); ?>" class="efeito" >Saiba mais</a>
									</div>

									<div class="col-md-7">
										<!-- DESCRIÇÃO -->
										<p class="texto">

										<?php
											$content = get_the_content();
											$conteudo = substr($content, 0, 80).'...';
											echo $conteudo;
										?>

										</p>
									</div>

								</div>
							</div>
						</div>
						<?php    endwhile; endif;  ?>

					</div>

				</div>

			</section>

		</div>
	</div>
	<!-- CARROSSEL -->
	<section class="videos">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<div class="bg"></div>
				</div>
				<div class="col-md-11">
					<p>#TripDeCasal</p>
					<!-- CARROSSEL DE VÍDIO -->
					<div class="carrossel">
						<div id="carrossel-videos" class="owl-Carousel">
							<?php
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCTaY2JKk0fhKUEXcBo2Voow&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {
		        	?>
							<!-- VÍDIO -->
							<div class="item">
								<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-play-circle"></i></a>

							</div>

							<?php
			       		}
					?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
