<?php
/**
 * Trip de Casal functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Trip_de_Casal
 */

if ( ! function_exists( 'tripdecasal_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tripdecasal_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Trip de Casal, use a find and replace
	 * to change 'tripdecasal' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'tripdecasal', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'tripdecasal' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tripdecasal_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'tripdecasal_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tripdecasal_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tripdecasal_content_width', 640 );
}
add_action( 'after_setup_theme', 'tripdecasal_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tripdecasal_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tripdecasal' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tripdecasal_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tripdecasal_scripts() {

	//FONT GOOGLE
	wp_enqueue_style( 'tripdecasal-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');

	//JAVA SCRIPT
	wp_enqueue_script( 'tripdecasal-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );
	wp_enqueue_script( 'tripdecasal-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'tripdecasal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'tripdecasal-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'tripdecasal-font-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	wp_enqueue_script( 'tripdecasal-jquery-facybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js' );


	wp_enqueue_script( 'tripdecasal-geral', get_template_directory_uri() . '/js/geral.js' );

	//CSS
	wp_enqueue_style( 'tripdecasal-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'tripdecasal-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style( 'tripdecasal-font-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'tripdecasal-jquery-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css');
	wp_enqueue_style( 'tripdecasal-style-site', get_template_directory_uri() . '/css/site.css');
	wp_enqueue_style( 'tripdecasal-style', get_stylesheet_uri() );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tripdecasal_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
// CONFGURAÇÕES VIA REDUX
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}
//PAGINAÇÃO
function pagination($pages = '', $range = 4){
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class=\"paginador\">";
         //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

 		$htmlPaginas = "";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 $htmlPaginas .= ($paged == $i)? '<li ><a href="' . get_pagenum_link($i) . '">' . $i . '</a></li>' : '<li ><a href="' . get_pagenum_link($i) . '" >' . $i . '</a></li>';
             }
         }

         if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '"><img src="' . get_template_directory_uri() . '/img/trocador_esq.png" /></a>';
         echo $htmlPaginas;
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '"><img src="' . get_template_directory_uri() . '/img/trocador_dir.png" /></a>';
         echo "</div>\n";
     }
}
// EXTENSÃO DA FUNÇÂO WP_GET_ARCHIVES
function extensaoFiltrosArchive($where,$args){

	$year		= isset($args['year']) 		? $args['year'] 	: '';
	$month		= isset($args['month']) 	? $args['month'] 	: '';
	$monthname	= isset($args['monthname']) ? $args['monthname']: '';
	$day		= isset($args['day']) 		? $args['day'] 		: '';
	$dayname	= isset($args['dayname']) 	? $args['dayname'] 	: '';
	if($year){
		$where .= " AND YEAR(post_date) = '$year' ";
		$where .= $month ? " AND MONTH(post_date) = '$month' " : '';
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($month){
		$where .= " AND MONTH(post_date) = '$month' ";
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($monthname){
		$where .= " AND MONTHNAME(post_date) = '$monthname' ";
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($day){
		$where .= " AND DAY(post_date) = '$day' ";
	}
	if($dayname){
		$where .= " AND DAYNAME(post_date) = '$dayname' ";
	}
	return $where;
}

add_filter( 'getarchives_where', 'extensaoFiltrosArchive',10,2);

add_action('init', 'remove_content_editor');

function remove_content_editor() {
    remove_post_type_support( 'cursos', 'editor' );
    remove_post_type_support( 'tratamentos', 'editor' );
}

	// FILTRA TIPOS DE LUGARES
	add_action('pre_get_posts', 'filtrosTiposConteudos');
	function filtrosTiposConteudos($query){

		if(!is_admin() && $query->is_main_query()){

			// SE POs
			if($query->query['post_type'] == 'lugares'){
				//var_dump($query);
				if(isset($_POST) && !empty($_POST)){

					$metas 	 = array();

					if(!empty($_POST['poFiltroPais'])){

						$metaPais  = array(
				                         'key' => 'Tripdecasal_lista_pais_lugar',
				                         'value' => $_POST['poFiltroPais'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaPais);
						//var_dump($metaPais);
					}

					if(!empty($_POST['poFiltroDestino'])){

						$metaDestino  = array(
				                         'key' => 'Tripdecasal_lista_destinos',
				                         'value' => $_POST['poFiltroDestino'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaDestino);
						//var_dump($metaDestino);
					}

					$query->set('meta_query',$metas);

				}

			}

		}
			return $query;

	}

	// FILTRA TIPOS DE GALERIA
	add_action('pre_get_posts', 'filtrosTipoGaleria');
	function filtrosTipoGaleria($query){

		if(!is_admin() && $query->is_main_query()){

			// SE POs
			if($query->query['post_type'] == 'galeria'){
				//var_dump($query);
				if(isset($_POST) && !empty($_POST)){

					$metas 	 = array();

					if(!empty($_POST['Filtro_banda'])){

						$metaPais  = array(
				                         'key' => 'Tripdecasal_lista_pais_galeria',
				                         'value' => $_POST['Filtropais_galeria'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaPais);
						//var_dump($metaPais);
					}

					if(!empty($_POST['FiltroDestino_galeria'])){

						$metaDestino  = array(
				                         'key' => 'Tripdecasal_lista_destinos_galeria',
				                         'value' => $_POST['FiltroDestino_galeria'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaDestino);
						//var_dump($metaDestino);
					}

					$query->set('meta_query',$metas);

				}

			}

		}
			return $query;

	}

	// FILTRA TIPOS DE BANDAS
	add_action('pre_get_posts', 'filtrosTipoBanda');
	function filtrosTipoBanda($query){

		if(!is_admin() && $query->is_main_query()){

			// SE POs
			if($query->query['post_type'] == 'trilha-sonora'){
				//var_dump($query);
				if(isset($_POST) && !empty($_POST)){

					$metas 	 = array();

					if(!empty($_POST['Filtro_banda'])){

						$metaPais  = array(
				                         'key' => 'Tripdecasal_lista_todas_banda',
				                         'value' => $_POST['Filtro_banda'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaPais);
						//var_dump($metaPais);
					}

					if(!empty($_POST['FiltroDestino_banda'])){

						$metaDestino  = array(
				                         'key' => 'Tripdecasal_lista_destinos_banda',
				                         'value' => $_POST['FiltroDestino_banda'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metaDestino);

					}

					$query->set('meta_query',$metas);

				}

			}

		}
			return $query;

	}

		// FILTRA TIPOS DE VÉIDEO
	add_action('pre_get_posts', 'filtrosTipoVideo');
	function filtrosTipoVideo($query){

		if(!is_admin() && $query->is_main_query()){

			// SE POs
			if($query->query['post_type'] == 'video'){
				//var_dump($query);
				if(isset($_POST) && !empty($_POST)){

					$metas 	 = array();

					if(!empty($_POST['Filtro_video'])){

						$metavideoPais  = array(
				                         'key' => 'Tripdecasal_lista_pais_video',
				                         'value' => $_POST['Filtro_video'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metavideoPais);
						//var_dump($metaPais);
					}

					if(!empty($_POST['FiltroDestino_video'])){

						$metavideoDestino  = array(
				                         'key' => 'Tripdecasal_lista_destinos_video',
				                         'value' => $_POST['FiltroDestino_video'],
				                         'compare' => '=='
				                         );

						array_push($metas, $metavideoDestino);
						//var_dump($metaDestino);
					}

					$query->set('meta_query',$metas);

				}

			}

		}
			return $query;

	}

    // VERSIONAMENTO DE FOLHAS DE ESTILO
	function my_wp_default_styles($styles)
	{
		$styles->default_version = "20160415";
	}
	add_action("wp_default_styles", "my_wp_default_styles");