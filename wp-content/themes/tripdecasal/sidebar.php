<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trip_de_Casal
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
global $configuracao;
?>

<div class="col-md-4">
					
	<!-- FORMULÁRIO DE CRITÉRIO DO BLOG -->
	<div class="form-arquivo-blog">						
		
		<p><i>Quer ler ainda mais matérias?</i></p>
		<span>Arquivo do blog</span>					
		<!-- <input placeholder="Critério de busca 1" type="text" class="criterio">
		<input placeholder="Critério de busca 2" type="text" class="criterio"> -->
		
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				/* Seleciona os anos no banco de dados */
				$anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
				$i = 0;
				foreach($anos as $ano) :									
			?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" class="criterio" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne<?php echo $i; ?>">
						Arquivo <?php echo $ano; ?> 
						</a>
					</h4>
				</div>
				<div id="collapseOne<?php echo $i; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
					<ul>
						<?php 
							$args = array(
											'type' => 'monthly',
											'echo' => 0,
											'year' => ''.$ano.'',
											);

							echo wp_get_archives( $args ); 
						?>
					</ul>
					</div>
				</div>
			</div>

			<?php $i++; endforeach; ?>
		</div>	
	
	</div>

	<!-- PROPAGANDAS -->
		<!-- PROPAGANDAS -->
	<div class="propagandas">
		<a href="<?php echo $configuracao['opt-link_propaganda1'] ?>" alt="" target="_blank"><img src="<?php echo $configuracao['opt-propaganda1']['url']; ?>" class="img-responsive" alt="propaganda"></a>
		<a href="<?php echo $configuracao['opt-link_propaganda2'] ?>" alt="" target="_blank"><img src="<?php echo $configuracao['opt-propaganda2']['url']; ?>" class="img-responsive" alt="propaganda"></a>
		<a href="<?php echo $configuracao['opt-link_propaganda3'] ?>" alt="" target="_blank"><img src="<?php echo $configuracao['opt-propaganda3']['url']; ?>" class="img-responsive" alt="propaganda"></a>						
	</div>

</div>