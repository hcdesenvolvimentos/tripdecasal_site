
	$(function(){

			$('#btnPrev').click(function(){
 				var carrossel_albuns = $("#carrossel-albuns").data('owlCarousel');
 				carrossel_albuns.prev();
				$('.center .item .foto-carrossel').trigger('click');
 			});
 			$('#btnNext').click(function(){

 				var carrossel_albuns = $("#carrossel-albuns").data('owlCarousel');
 				carrossel_albuns.next();
				$('.center .item .foto-carrossel').trigger('click');
 			});

		/*******************************************************
        *  CARROSSEL PÁGINA INICIAL
		*******************************************************/

		$(document).ready(function() {

	  		$("#carrossel-videos").owlCarousel({

				items : 5,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:5
			            }

			        }
	  		});
	  		var carrossel_videos = $("#carrossel-videos").data('owlCarousel');
			$('.navegacaovideosFrent').click(function(){ carrossel_videos.prev(); });
			$('.navegacaovideosTras').click(function(){ carrossel_videos.next(); });

	 	});

  		/*******************************************************
        *  CARROSSEL PÁGINA ÁLBUNS
		*******************************************************/

		$(document).ready(function() {

	  		$("#carrossel-instagram").owlCarousel({

				items : 3,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:3
			            }

			        }
	  		});
	  		var carrossel_instagram = $("#carrossel-instagram").data('owlCarousel');
			$('.navegacaoinstagramFrent').click(function(){ carrossel_instagram.prev(); });
			$('.navegacaoinstagramTras').click(function(){ carrossel_instagram.next(); });

	 	});

		// //FUNÇÃO PARA ABRIR MODAL E RECEBER INFORMAÇÕES DO ÁLBUM
		$(document).ready(function() {

			$('.link-album').click(function(e){
				e.preventDefault();
				var tituloAlbum = $(this).attr('data-title');
				var todasfotos = $(this).attr('data-fotos-album');
				var fotodestacada = $(this).attr('data-foto-destacada');

				$('#fotodestaque').css('background-image', 'url(' + fotodestacada + ')');
				$('#myModal').modal('show');

				$("#carrossel-albuns").css('visibility','hidden');
				function explode(){
				 	$("#carrossel-albuns").owlCarousel({
							loop:true,
						    margin:10,
						    nav:false,
						    mouseDrag: false,
						    center: true,
						    responsive:{
						        0:{
						            items:1
						        },
						        600:{
						            items:3
						        },
						        1000:{
						            items:3
						        }
						    }
				  		});
				  		$("#carrossel-albuns").css('visibility','visible');
				  		var carrossel_albuns = $("#carrossel-albuns").data('owlCarousel');
						$('.navegacaoalbunsFrent').click(function(){ carrossel_albuns.prev(); });
						$('.navegacaoalbunsTras').click(function(){ carrossel_albuns.next(); });
				}
				setTimeout(explode, 500);
			});

		});



		// FUNÇÃO PARA MUDAR FOTO DA MODAL
 		$(document).ready(function() {

			$('.foto-carrossel').click(function(e){
			var urlImg = $(this).attr('data-url');
			var nome = $(this).attr('data-titulo');

			$('#fotodestaque').css('background-image', 'url(' + urlImg + ')');
			$('#tituloPost').text(nome);




			});
		});


	 	// PEGANDO ALTURA DA DIV COM PONTILHADO
		 	var alturaDiv = $('#altura').height();
		 	var largura = $(window).width();
		 	var alturasidebar = $('.bg-menu').height();

		 	$('#pon').css({"height":alturaDiv});


	 	// TAMANHO TRINGULO HEADER
			var Height = $('#caixadetexto').height();
			caixaHeight =  (Height/2);
			heightTriangulo = (caixaHeight/2) - 0.5;
			$('#caixadetexto').append('<style>#caixadetexto:before{right: -'+heightTriangulo+'px!important;border-width:'+caixaHeight+'px 0 '+caixaHeight+'px '+caixaHeight/2+'px;}</style>');

		// FANCYBOX
			$("a#example1").fancybox({
				'titleShow'     : false
			});

			$('a.AlpinePhotoTiles-link').fancybox({
				'titleShow'     : false
			});

		// SCRIPT NOME E FOTO DAS PÁGINAS
		$(window).load(function(){
			var link = $('.info-pagina').attr('data-link');
			var title = $('.info-pagina').attr('data-title');
			var frase = $('.info-pagina').attr('data-frase');
			$('#foto-top').css({"background":"url("+link+")"});
			$('#titulo').text(title);
			$('#frase').text(frase);
		 });

		//VERIFICAÇÃO DO SELECT
		$('#selectPais').on('change', function(event ) {
		   var value = $(this).val();

		   $("#selectDestino option").each(function(){
		   		if($(this).attr("data-idDestino") != value){
		   			$(this).hide();
		   		}else{
		   			$(this).show();
		   		}
		   });
		});

		$('#selectPaisvideo').on('change', function(event ) {
		   var value = $(this).val();

		   $("#selectDestinovideo option").each(function(){
		   		if($(this).attr("data-idDestino") != value){
		   			$(this).hide();
		   		}else{
		   			$(this).show();
		   		}
		   });
		});



		// $('a#ancora-formulario').click(function() {
		// if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		// 		var target = $(this.hash);
		// 		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		// 		if (target.length) {
		// 			$('html,body').animate({
		// 				scrollTop: target.offset().top
		// 			}, 1000);
		// 			return false;
		// 		}
		// 	}
		// });

		$(document).ready(function(){
		    var speed = 1000;

		    // check for hash and if div exist... scroll to div
		    var hash = window.location.hash;
		    if($(hash).length) scrollToID(hash, speed);

		    // scroll to div on nav click
		    $('.nav a').click(function (e) {
		        e.preventDefault();
		        var id = $(this).data('id');
		        var href = $(this).attr('href');
		        if(href === '#'){
		            scrollToID(id, speed);
		        }else{
		            window.location.href = href;
		        }

		    });
	    	$('a[href*=#]:not([href=#])').click(function() {
	    	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	      var target = $(this.hash);
	    	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    	      if (target.length) {
	    	        $('html,body').animate({
	    	          scrollTop: target.offset().top
	    	        }, 1000);
	    	        return false;
	    	      }
	    	    }
	    	  });
		})

		function scrollToID(id, speed) {
		    var offSet = 70;
		    var obj = $(id).offset();
		    var targetOffset = obj.top - offSet;
		    $('html,body').animate({ scrollTop: targetOffset }, speed);
		}






	});
