<?php

/**
 * Plugin Name: Base Trip de Casal
 * Description: Controle base do tema Trip de Casal.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseTripdecasal () {

		// TIPOS DE CONTEÚDO
		conteudosTripdecasal();

		// TAXONOMIA
		taxonomiaTripdecasal();

		// META BOXES
		metaboxesTripdecasal();

		// SHORTCODES
		// shortcodesTripdecasal();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerTripdecasal();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosTripdecasal (){

		// TIPOS DE CONTEÚDO				
		tipoPais();						
		
		tipoDestino();						

		tipoLugares();

		tipoParceiro();	

		tipoGaleria();

		tipoTrilhasonora();

		tipoViajandomusica();

		tipoBanda();
		
		tipoVideo();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {
				
				case 'país':
					$titulo = 'Nome do país';
				break;

				case 'destino':
					$titulo = 'Nome do destino';
				break;
				
				case 'lugar':
					$titulo = 'Nome do lugar';
				break;

				case 'parceiro':
					$titulo = 'Nome do parceiro';
				break;				

				case 'galeria':
					$titulo = 'Nome da galeria';
				break;			

				case 'trilha sonora':
					$titulo = 'Nome da trilha sonora';
				break;

				case 'banda':
					$titulo = 'Nome da banda';
				break;	

				case 'video':
					$titulo = 'Nome do vídeo';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}	

	// CUSTOM POST TYPE PAIS
	function tipoPais() {

		$rotulosPais = array(
								'name'               => 'País',
								'singular_name'      => 'país',
								'menu_name'          => 'País',
								'name_admin_bar'     => 'País',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo país',
								'new_item'           => 'Novo país',
								'edit_item'          => 'Editar país',
								'view_item'          => 'Ver país',
								'all_items'          => 'Todos os países',
								'search_items'       => 'Buscar país',
								'parent_item_colon'  => 'Dos países',
								'not_found'          => 'Nenhum país cadastrado.',
								'not_found_in_trash' => 'Nenhum país na lixeira.'
							);

		$argsPais 	= array(
								'labels'             => $rotulosPais,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-admin-site',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'pais' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('pais', $argsPais);

	}

	// CUSTOM POST TYPE DESTINO
	function tipoDestino() {

		$rotulosDestino = array(
								'name'               => 'Destino',
								'singular_name'      => 'destino',
								'menu_name'          => 'Destinos',
								'name_admin_bar'     => 'Destino',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destino',
								'new_item'           => 'Novo destino',
								'edit_item'          => 'Editar destino',
								'view_item'          => 'Ver destino',
								'all_items'          => 'Todos os destinos',
								'search_items'       => 'Buscar destino',
								'parent_item_colon'  => 'Dos destinos',
								'not_found'          => 'Nenhum destino cadastrado.',
								'not_found_in_trash' => 'Nenhum destino na lixeira.'
							);

		$argsDestino	= array(
								'labels'             => $rotulosDestino,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-location',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destino' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destino', $argsDestino);

	}	

	// CUSTOM POST TYPE PARCEIROS
	function tipoParceiro() {

		$rotulosParceiro = array(
								'name'               => 'Parceiro',
								'singular_name'      => 'parceiro',
								'menu_name'          => 'Parceiros',
								'name_admin_bar'     => 'Parceiros',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo parceiro',
								'new_item'           => 'Novo parceiro',
								'edit_item'          => 'Editar parceiro',
								'view_item'          => 'Ver parceiro',
								'all_items'          => 'Todos os parceiro',
								'search_items'       => 'Buscar parceiro',
								'parent_item_colon'  => 'Dos parceiro',
								'not_found'          => 'Nenhum parceiro cadastrado.',
								'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
							);

		$argsParceiro 	= array(
								'labels'             => $rotulosParceiro,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'parceiro' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiro', $argsParceiro);

	}

	// CUSTOM POST TYPE VIAJANDO NA MÚSICA
	function tipoViajandomusica() {

		$rotulosViajandomusica = array(
								'name'               => 'Banda',
								'singular_name'      => 'banda',
								'menu_name'          => 'Bandas',
								'name_admin_bar'     => 'Bandas',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova banda',
								'new_item'           => 'Nova banda',
								'edit_item'          => 'Editar banda',
								'view_item'          => 'Ver banda',
								'all_items'          => 'Todas as banda',
								'search_items'       => 'Buscar banda',
								'parent_item_colon'  => 'Das banda',
								'not_found'          => 'Nenhuma banda cadastrado.',
								'not_found_in_trash' => 'Nenhuma banda na lixeira.'
							);

		$argsViajandomusica 	= array(
								'labels'             => $rotulosViajandomusica,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-format-audio',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'banda' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('banda', $argsViajandomusica);

	}

	// CUSTOM POST TYPE GALERIA
	function tipoGaleria() {

		$rotulosGaleria = array(
								'name'               => 'Galeria',
								'singular_name'      => 'galeria',
								'menu_name'          => 'Galerias',
								'name_admin_bar'     => 'Galerias',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova galeria',
								'new_item'           => 'Nova galeria',
								'edit_item'          => 'Editar galeria',
								'view_item'          => 'Ver galeria',
								'all_items'          => 'Todas as galerias',
								'search_items'       => 'Buscar galeria',
								'parent_item_colon'  => 'Das galeria',
								'not_found'          => 'Nenhuma galeria cadastrada.',
								'not_found_in_trash' => 'Nenhuma galeria na lixeira.'
							);

		$argsGaleria 	= array(
								'labels'             => $rotulosGaleria,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-format-gallery',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'galeria' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('galeria', $argsGaleria);

	}

	// CUSTOM POST TYPE VÍDEOS
	function tipoVideo() {

		$rotulosVideo = array(
								'name'               => 'Vídeo',
								'singular_name'      => 'vídeo',
								'menu_name'          => 'Vídeos',
								'name_admin_bar'     => 'Vídeos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo vídeo',
								'new_item'           => 'Novo vídeo',
								'edit_item'          => 'Editar vídeo',
								'view_item'          => 'Ver vídeo',
								'all_items'          => 'Todos os vídeos',
								'search_items'       => 'Buscar vídeo',
								'parent_item_colon'  => 'Dos vídeos',
								'not_found'          => 'Nenhum vídeo cadastrado.',
								'not_found_in_trash' => 'Nenhum vídeo na lixeira.'
							);

		$argsVideo 	= array(
								'labels'             => $rotulosVideo,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-editor-video',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'video' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('video', $argsVideo);

	}

	// CUSTOM POST TYPE LUGARES
	function tipoLugares() {

		$rotulosLugares = array(
								'name'               => 'Lugar',
								'singular_name'      => 'lugar',
								'menu_name'          => 'Lugares',
								'name_admin_bar'     => 'Lugares',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo lugar',
								'new_item'           => 'Novo lugar',
								'edit_item'          => 'Editar lugar',
								'view_item'          => 'Ver lugar',
								'all_items'          => 'Todos os Lugares',
								'search_items'       => 'Buscar lugar',
								'parent_item_colon'  => 'Dos lugares',
								'not_found'          => 'Nenhum lugar cadastrado.',
								'not_found_in_trash' => 'Nenhum lugar na lixeira.'
							);

		$argsLugares 	= array(
								'labels'             => $rotulosLugares,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-location-alt',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'lugares' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('lugares', $argsLugares);

	}

	// CUSTOM POST TYPE TRILHA SONORA
	function tipoTrilhasonora() {

		$rotulosTrilhasonora = array(
								'name'               => 'Trilha sonora',
								'singular_name'      => 'trilha sonora',
								'menu_name'          => 'Trilhas sonora',
								'name_admin_bar'     => 'trilhas sonora',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova trilha sonora',
								'new_item'           => 'Nova trilha sonora',
								'edit_item'          => 'Editar trilha sonora',
								'view_item'          => 'Ver trilha sonora',
								'all_items'          => 'Todas as trilha sonora',
								'search_items'       => 'Buscar trilha sonora',
								'parent_item_colon'  => 'Das trilhas sonora',
								'not_found'          => 'Nenhuma trilha sonora cadastrada.',
								'not_found_in_trash' => 'Nenhuma trilha sonora na lixeira.'
							);

		$argsTrilhasonora 	= array(
								'labels'             => $rotulosTrilhasonora,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-format-video',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'trilha-sonora' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('trilha-sonora', $argsTrilhasonora);

	}

	// CUSTOM POST TYPE BANDA
	function tipoBanda() {

		$rotulosBanda = array(
								'name'               => 'Banda',
								'singular_name'      => 'banda',
								'menu_name'          => 'Bandas',
								'name_admin_bar'     => 'bandas',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova banda',
								'new_item'           => 'Nova banda',
								'edit_item'          => 'Editar banda',
								'view_item'          => 'Ver banda',
								'all_items'          => 'Todas as bandas',
								'search_items'       => 'Buscar banda',
								'parent_item_colon'  => 'Das bandas',
								'not_found'          => 'Nenhuma banda cadastrada.',
								'not_found_in_trash' => 'Nenhuma banda na lixeira.'
							);

		$argsBanda 	= array(
								'labels'             => $rotulosBanda,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-format-audio',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'banda' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('banda', $argsBanda);

	}
	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaTripdecasal () {

		taxonomiaCategoriaParceiro();
		
		taxonomiaCategoriaLugares();

	}
		// TAXONOMIA DE PARCEIROS
		function taxonomiaCategoriaParceiro() {

			$rotulosCategoriaParceiro = array(
												'name'              => 'Categorias de parceiro',
												'singular_name'     => 'Categoria de parceiro',
												'search_items'      => 'Buscar categorias de parceiro',
												'all_items'         => 'Todas categorias de parceiro',
												'parent_item'       => 'Categoria de parceiro pai',
												'parent_item_colon' => 'Categoria de parceiro pai:',
												'edit_item'         => 'Editar categoria de parceiro',
												'update_item'       => 'Atualizar categoria de parceiro',
												'add_new_item'      => 'Nova categoria de parceiro',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de parceiro',
											);

			$argsCategoriaParceiro 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaParceiro,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-parceiro' ),
											);

			register_taxonomy( 'categoriaParceiro', array( 'parceiro' ), $argsCategoriaParceiro );

		}
		// TAXONOMIA DE LUGARES
		function taxonomiaCategoriaLugares() {

			$rotulosCategoriaLugares = array(
												'name'              => 'Categorias de lugares',
												'singular_name'     => 'Categoria de lugar',
												'search_items'      => 'Buscar categorias de lugar',
												'all_items'         => 'Todas categorias de lugar',
												'parent_item'       => 'Categoria de lugar pai',
												'parent_item_colon' => 'Categoria de lugar pai:',
												'edit_item'         => 'Editar categoria de lugar',
												'update_item'       => 'Atualizar categoria de lugar',
												'add_new_item'      => 'Nova categoria de lugar',
												'new_item_name'     => 'Nova lugares',
												'menu_name'         => 'Categorias de lugares',
											);

			$argsCategoriaLugares 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaLugares,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-lugares' ),
											);

			register_taxonomy( 'categoriaLugares', array( 'lugares' ), $argsCategoriaLugares );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesTripdecasal(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}
		function registraMetaboxes( $metaboxes ){

			$prefix = 'Tripdecasal_';	

			// LISTA PAÍSES
				$listarpais = array();
				$loopListaPais = new WP_Query( array( 'post_type' => 'pais', 'order' => 'ASC', 'orderby' => 'name', 'posts_per_page' => -1 ) );
				$i = 0;
				while ( $loopListaPais->have_posts() ) : $loopListaPais->the_post();
				global $post;
				$listarpais[$post->ID] = get_the_title();
				endwhile;
				wp_reset_query();	

			// LISTAR DESTINO
				$listarDestinos = array();
				$loopListaDestinos = new WP_Query( array( 'post_type' => 'destino', 'order' => 'ASC', 'orderby' => 'name', 'posts_per_page' => -1 ) );
				$i = 0;
				while ( $loopListaDestinos->have_posts() ) : $loopListaDestinos->the_post();
				 global $post;
				 $listarDestinos[$post->ID] = get_the_title();
				endwhile;
				wp_reset_query();

			// LISTAR BANDAS
				$listarBandas = array();
				$loopListaBandas = new WP_Query( array( 'post_type' => 'banda', 'order' => 'ASC', 'orderby' => 'name', 'posts_per_page' => -1 ) );
				$i = 0;
				while ( $loopListaBandas->have_posts() ) : $loopListaBandas->the_post();
				 global $post;
				 $listarBandas[$post->ID] = get_the_title();
				endwhile;
				wp_reset_query();

			// METABOX DE PARCEIROS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPost',
				'title'			=> 'Detalhes do post',
				'pages' 		=> array( 'post' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Frase: ',
						'id'    => "{$prefix}frase_post",
						'desc'  => '',
						'type'  => 'text'
					),					

				),

			);

			// METABOX DE PARCEIROS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxParceiros',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'parceiro' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link do parceiro: ',
						'id'    => "{$prefix}link_parceiro",
						'desc'  => '',
						'type'  => 'text'
					),					

				),

			);

			// METABOX DE BANDAS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxBandas',
				'title'			=> 'Detalhes da banda',
				'pages' 		=> array( 'banda' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link da banda: ',
						'id'    => "{$prefix}link_banda",
						'desc'  => '',
						'type'  => 'text'
					),					

				),

			);			

			// METABOX DE GALERIA
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxGaleria',
				'title'			=> 'Detalhes da galeria',
				'pages' 		=> array( 'galeria' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(


					array(
						'name'  => 'Galeria de fotos: ',
						'id'    => "{$prefix}image_galeria",
						'desc'  => '',
						'type'  => 'image'
					),

					array(
					 'name' => 'País',
					 'id' => "{$prefix}lista_pais_galeria",
					 'type' => 'select',
					 'options' => $listarpais,
					 'multiple' => false
					 ),	

					array(
					 'name' => 'Destino',
					 'id' => "{$prefix}lista_destinos_galeria",
					 'type' => 'select',
					 'options' => $listarDestinos,
					 'multiple' => false
					 ),				

				),

			);

			// METABOX DE VÍDEOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxVideo',
				'title'			=> 'Detalhes do vídeo',
				'pages' 		=> array( 'video' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(					

					array(
					'name'  => 'Link do vídeo: ',
					'id'    => "{$prefix}link_video",
					'desc'  => '',
					'type'  => 'text'
					),

					array(
					 'name' => 'País',
					 'id' => "{$prefix}lista_pais_video",
					 'type' => 'select',
					 'options' => $listarpais,
					 'multiple' => false
					 ),	

					array(
					 'name' => 'Destino',
					 'id' => "{$prefix}lista_destinos_video",
					 'type' => 'select',
					 'options' => $listarDestinos,
					 'multiple' => false
					 ),				

				),

			);


			// METABOX DE IMAGENS DA TRILHA SONORA
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxtrilhasonora',
				'title'			=> 'Detalhes do álbum',
				'pages' 		=> array( 'trilha-sonora' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto da capa: ',
						'id'    => "{$prefix}image_capa",
						'desc'  => '',
						'type'  => 'image',
						//'max_file_uploads'  => 1
					),
					array(
						'name'  => 'Script do álbum: ',
						'id'    => "{$prefix}album_script",
						'desc'  => '',
						'type'  => 'text'
			
					),
					array(
					 'name' => 'Destino',
					 'id' => "{$prefix}lista_destinos_banda",
					 'type' => 'select',
					 'options' => $listarDestinos,
					 'multiple' => false
					 ),	

					 array(
					 'name' => 'Bandas',
					 'id' => "{$prefix}lista_todas_banda",
					 'type' => 'select',
					 'options' => $listarBandas,
					 'multiple' => true
					 ),										

				),

			);	
			

			// METABOX DE LOOP DE PAÍSES
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPais',
				'title'			=> 'Detalhe do destino',
				'pages' 		=> array( 'destino' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
					 'name' => 'País',
					 'id' => "{$prefix}lista_pais",
					 'type' => 'select',
					 'options' => $listarpais,
					 'multiple' => false
					 )	
					

				),		

			);

			// METABOX DE LOOP DE LUGARES
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLugares',
				'title'			=> 'Detalhe do lugar',
				'pages' 		=> array( 'lugares' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
					 'name' => 'Destino',
					 'id' => "{$prefix}lista_destinos",
					 'type' => 'select',
					 'options' => $listarDestinos,
					 'multiple' => false
					 ),
					 array(
					 'name' => 'País',
					 'id' => "{$prefix}lista_pais_lugar",
					 'type' => 'select',
					 'options' => $listarpais,
					 'multiple' => false
					 ),
					array(
						'name'  => 'Link para o álbum de foto : ',
						'id'    => "{$prefix}link_albundefoto",
						'desc'  => '',
						'type'  => 'text',
						'clone' =>'true'
			
					),									

				),
			);

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesTripdecasal(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerTripdecasal(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseTripdecasal');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseTripdecasal();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );